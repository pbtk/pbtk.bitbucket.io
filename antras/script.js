$(document).ready(function() {
  $('#fullpage').fullpage({
    licenseKey: 'CFC1B460-BD2C4EC3-B408FCB5-A08D1518',
    sectionsColor: ['#fdfffc', '#2ec4b6', '#ff9f1c'],
    menu: '#menu',
    scrollOverflow: true,
    controlArrows: false,
    showActiveTooltip: true,
    slidesNavigation: true,
    slidesNavPosition: 'top'
  });

  fullpage_api.setFitToSection(false);
});
